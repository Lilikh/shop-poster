const posters=[{
    id:1,
    title:"nature poster",
    category:"nature",
    price:15.99,
    img:"img/nature7.jpg",
    description:"Rose blommor på en "

},
{
    id:2,
    title:"nature poster",
    category:"nature",
    price:15.99,
    img:"img/nature2.jpg",
    description:"Rose blommor på en "

},
{
    id:3,
    title:"nature poster",
    category:"nature",
    price:15.99,
    img:"img/nature3.jpg",
    description:"Rose blommor på en "

},
{
    id:4,
    title:"nature poster",
    category:"nature",
    price:15.99,
    img:"img/nature4.jpg",
    description:"Rose blommor på en "

},
{
    id:5,
    title:"nature poster",
    category:"nature",
    price:15.99,
    img:"img/nature5.jpg",
    description:"lorem nbj knckf jkhjkchf knjcjkf "

},
{
    id:6,
    title:"nature poster",
    category:"nature",
    price:15.99,
    img:"img/nature6.jpg",
    description:"Rose blommor på en "

},
{
    id:7,
    title:"nature poster",
    category:"nature",
    price:15.99,
    img:"img/nature7.jpg",
    description:"Rose blommor på en "

},
{
    id:8,
    title:"bird poster",
    category:"birds",
    price:15.99,
    img:"img/bird1.jpg",
    description:"Rose blommor på en "

},
{
    id:9,
    title:"bird poster",
    category:"birds",
    price:15.99,
    img:"img/bird2.jpg",
    description:"Rose blommor på en "

},
{
    id:10,
    title:"bird poster",
    category:"birds",
    price:15.99,
    img:"img/bird3.jpg",
    description:"Rose blommor på en "

},
{
    id:11,
    title:"bird poster",
    category:"birds",
    price:15.99,
    img:"img/bird4.jpg",
    description:"Rose blommor på en "

},
{
    id:12,
    title:"bird poster",
    category:"birds",
    price:15.99,
    img:"img/bird5.jpg",
    description:"Rose blommor på en "

},
{
    id:13,
    title:"bird poster",
    category:"birds",
    price:15.99,
    img:"img/bird6.jpg",
    description:"Rose blommor på en "

},
{
    id:14,
    title:"city poster",
    category:"city",
    price:15.99,
    img:"img/city1.jpg",
    description:"Rose blommor på en "

},
{
    id:15,
    title:"city poster",
    category:"city",
    price:15.99,
    img:"img/city2.jpg",
    description:"Rose blommor på en "

},
{
    id:16,
    title:"city poster",
    category:"city",
    price:15.99,
    img:"img/city4.jpg",
    description:"Rose blommor på en "

},
{
    id:17,
    title:"city poster",
    category:"city",
    price:15.99,
    img:"img/city5.jpg",
    description:"Rose blommor på en "

},
{
    id:18,
    title:"animal poster",
    category:"animals",
    price:15.99,
    img:"img/animal8.jpg",
    description:"Rose blommor på en "

},
{
    id:19,
    title:"animal poster",
    category:"animals",
    price:15.99,
    img:"img/animal7.jpg",
    description:"Rose blommor på en "

},
{
    id:20,
    title:"animal poster",
    category:"animals",
    price:15.99,
    img:"img/animal2.webp",
    description:"Rose blommor på en "

},
{
    id:21,
    title:"animal poster",
    category:"animals",
    price:15.99,
    img:"img/animal3.webp",
    description:"Rose blommor på en "

},
{
    id:22,
    title:"animal poster",
    category:"animals",
    price:15.99,
    img:"img/animal5.webp",
    description:"Rose blommor på en "

}

];

const sectionCenter= document.querySelector(".section-center");
const container = document.querySelector(".btn-container");


window.addEventListener("DOMContentLoaded",()=>{
    
})
function displyPosterItem(posters){
    let displayPosters=""
    posters.map((poster)=>{
        displayPosters +=
        displayPosters=`<artticle class="poster-item">
                <img class="photo" src=${poster.img} alt=${poster.description}>
                <div class="item-info">
                    <div class="item-header">
                        <h4>${poster.category}</h4>
                    </div>
                    <p class="item-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur quibusdam, ad dolorum natus incidunt repudiandae provident id dolorem, fugiat labore blanditiis eaque commodi corporis. Debitis!</p>
                    <h4 class="item-price">$${poster.price}</h4>
                </div>  
            </artticle>`;
       
    })
    sectionCenter.innerHTML=displayPosters;
}
displyPosterItem(posters);


function displayButton(posters){

    const categories=["all", ...new Set(posters.map(poster=>poster.category))]

    let buttons=""
    categories.map((category)=>{
        console.log(category);
        buttons +=
        `<button class="filter-btn" data-id=${category}>${category}</button>`
    })
    container.innerHTML=buttons;

    const filterButtons=container.querySelectorAll(".filter-btn");
    filterButtons.forEach((button)=>{
        button.addEventListener("click", (event)=>{
            //console.log(event.currentTarget.dataset.id);
            const category=event.currentTarget.dataset.id;
            const posterCategory= posters.filter((poster)=>{
                if(poster.category===category){
                    return poster;
                }
            })
            if(category==="all"){
                displyPosterItem(posters);
            }else{
                displyPosterItem(posterCategory)
            }
        })
    })

}
displayButton(posters);